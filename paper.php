<?php
$name = "naveen";
$age = 12;
$intVar = 10;
$x = 12.5;
$isFree = true; //false,true
//Arrays
$firstArray = [12,13.5,15,"hi",1,9];
//echo $firstArray[3];
$userInfo = [
'name' => 'Tom',
 'age' => 10, 
 'animal' => 'cat'
 ];
// echo $userInfo['animal'];

var_dump($userInfo);
print_r($userInfo);

$userInfo['country'] = 'USA';
$userInfo['age'] = 15; //information will be overwrited in the above array $userInfo[1] = 'hi there'; //information will be overwrited in the above array

$userInfo[] = 'element added at end'; // information will be add at the end
var_dump($userInfo);

$userInfo[20] = 'missing 20'; //information will be directly added to address 20 leaving middle ones empty
var_dump($userInfo);

echo $userInfo['name'];
$age = 12;
$doubleQuoteString = "Age's is \"$age\"";
$singleQuoteString = 'Age\'s is "$age"';
 echo "<br>"; //shows new line on the browser
 echo $doubleQuoteString;
 echo "<br>";
 echo $singleQuoteString;







 ?>
 